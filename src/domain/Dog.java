package domain;


/**
 * ${DESCRIPTION}
 *
 * @author 旭旭
 * @create 2018-08-05 20:19
 **/
public class Dog {
    private String name = "哮天犬";
    private int food = 10;

    public Dog() {
    }

    public Dog(String name, int food) {
        this.name = name;
        this.food = food;
    }

    public static void bark(Dog dog){
        System.out.println(dog + "在叫！");
    }

    public int eat(Dog this,int num){
        System.out.println("吃了" + num + "斤狗粮");
        this.food -= num;
        return this.food;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }
}
