package function.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CreateStream {

    public static void main(String[] args) {
//        collectionCreate();
//        arrayCreate();
//        numCreate();
        selfCreate();
    }

    /**
     * 集合创建
     */
    public static void collectionCreate(){
        List<String> list = new ArrayList<>();
        Stream<String> stream = list.stream();
        Stream<String> parallelStream = list.parallelStream();
    }

    /**
     * 数组创建
     */
    public static void arrayCreate(){
        Integer[] array = new Integer[5];
        Stream<Integer> stream = Arrays.stream(array);
    }

    /**
     * 数字创建
     */
    public static void numCreate(){
        IntStream.of(1,2,3);
        IntStream.rangeClosed(1,10);
        new Random().ints().limit(10);
    }

    /**
     * 自己创建
     */
    public static void selfCreate(){
        Random random = new Random();
        Stream.generate(random::nextInt).limit(20);
        Stream.iterate(2, (x) -> x*2).limit(10).forEach(System.out::println);
    }
}
