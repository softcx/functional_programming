package function.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * ${DESCRIPTION}
 *
 * @author 旭旭
 * @create 2018-08-12 23:17
 **/
public class StreamDemo {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for(int i = 1 ; i <= 10000; i++){
            list.add(random.nextInt());
        }
        Long oldTime = System.currentTimeMillis();
//        outerSort(list);
        innerSort(list);
        Long time = System.currentTimeMillis();
        System.out.println(time - oldTime);
    }

    //外部迭代方法
    public static void outerSort(List<Integer> list){
        for(int i = 0 ; i < list.size() -1 ; i++){
            for(int j = 0 ; j < list.size() -1 ; j++){
                if(list.get(j) > list.get(j + 1)){
                    int t = list.get(j + 1);
                    list.set(j + 1,list.get(j));
                    list.set(j,t);
                }
            }
        }
    }

    //内部迭代方法
    public static void innerSort(List<Integer> list){
        //流的创建操作 list.stream()  返回流
        //流的中间操作 map sorted     返回流
        //流的终止操作 forEach        返回产品
        list.stream().map(StreamDemo::lazyEvaluation).sorted().forEach(x->{});
//        list.stream().map(StreamDemo::lazyEvaluation).sorted();

    }

    /**
     * 终止操作没有调用下，中间操作不会执行。
     */
    public static Integer lazyEvaluation(Integer i){
        System.out.print("调用了加2操作");
        return i + 2;
    }

}
