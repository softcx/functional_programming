package function.stream;

import domain.Student;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 收集器
 *
 * @author 旭旭
 * @create 2018-08-18 23:43
 **/
public class CollectorsStream {

    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("一号",7,true,"一年级"));
        students.add(new Student("二号",8,true,"二年级"));
        students.add(new Student("三号",8,false,"二年级"));
        students.add(new Student("四号",9,true,"三年级"));
        students.add(new Student("五号",7,false,"一年级"));
        students.add(new Student("六号",8,true,"二年级"));
        students.add(new Student("七号",10,true,"四年级"));

//        dataToList(students);
//        summary(students);
//        partitioning(students);
        group(students);
    }

    /**
     * 获取某一数据的集合
     */
    public static void dataToList(List<Student> students){
        List<Integer> list = students.stream().map(Student::getAge).collect(Collectors.toList());
        System.out.println(list);
    }

    /**
     * 获取某一数据的汇总值
     */
    public static void summary(List<Student> students){
        IntSummaryStatistics collect = students.stream().collect(Collectors.summarizingInt(Student::getAge));
        System.out.println(collect);
    }

    /**
     * 根据某一数据分类
     */
    public static void partitioning(List<Student> students){
        Map<Boolean, List<Student>> collect = students.stream().collect(Collectors.partitioningBy(x -> x.isGender()));
        System.out.println(collect);
    }

    /**
     * 根据某一数据分组
     */
    public static void group(List<Student> students){
        Map<String, Long> collect = students.stream().collect(Collectors.groupingBy(Student::getGrade, Collectors.counting()));
        System.out.println(collect);
    }
}
