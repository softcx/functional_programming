package function;

/**
 * ${DESCRIPTION}
 *
 * @author 旭旭
 * @create 2018-08-19 0:34
 **/
@FunctionalInterface
interface IMethod{
    public void method();
}

public class LambdaDemo {
    public static void main(String[] args){
        IMethod iMethod = () -> {};
    }

    public static void useLambda(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.print("123");
            }
        });
        thread.start();

        //lambda
        new Thread(() -> System.out.print("123")).start();
    }
}
