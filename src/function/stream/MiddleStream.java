package function.stream;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 流中间操作
 *
 * @author 旭旭
 * @create 2018-08-14 23:25
 **/
public class MiddleStream {
    public static void main(String[] args) {
//        mapOrMapToXXX();
//        flatMap();
//        peek();
//        distinct();
//        sort();
        limitSkip();
    }

    /**
     * map操作    A -> B
     * filter操作
     */
    public static void mapOrMapToXXX(){
        String s = "my name is 007";
        Stream.of(s.split(" ")).map(String::length).forEach(System.out::println);
        System.out.println("-------------");
        Stream.of(s.split(" ")).filter(x -> x.length() > 2).mapToDouble(x -> x.length()).forEach(System.out::println);
    }

    /**
     * flatMap操作 A -> B list
     * IntStream/LongStream 并不是stream的子类需要进行装箱
     */
    public static void flatMap(){
        String s = "my name is 007";
        Stream.of(s.split(" ")).flatMap(x -> x.chars().boxed()).forEach(x -> System.out.println((char)x.intValue()));
    }

    /**
     * peek 要类型对应
     */
    public static void peek(){
        IntStream.of(new int[]{1,2,3,4,5}).peek(System.out::println).forEach(x->{});
    }

    /**
     * distinct
     */
    public static void distinct(){
        IntStream.of(new int[]{1,3,3,4,5}).distinct().forEach(System.out::println);
    }

    /**
     * sort
     */
    public static void sort(){
        IntStream.of(new int[]{5,4,3,1,2}).sorted().forEach(System.out::println);
    }

    /**
     * limitSkip
     */
    public static void limitSkip(){
        IntStream.of(new int[]{1,2,3,4,5,6,7,8}).skip(2).limit(2).forEach(System.out::println);
    }
}
