package function;

/**
 * 类型推断
 *
 * @author 旭旭
 * @create 2018-08-12 0:42
 **/
@FunctionalInterface
interface IMath{
    int add(int x,int y);
}

@FunctionalInterface
interface IMath2{
    int add(int x,int y);
}

public class TypeDemo {
    public static void main(String[] args) {
        //变量类型定义
        IMath iMath = (x,y) -> x + y;

        //数组定义
        IMath[] arrIMath = {(x,y) -> x + y};

        //强转
        Object obj = (IMath)(x,y) -> x + y;

        //通过方法定义
        IMath iMathByMethod = createLambda();

        //参数定义
        //同类型重载
        useLambda((x,y) -> x + y);


    }

    private static void useLambda(IMath iMath){

    }

//    private static void userLambda(IMath2 iMath2){
//
//    }

    private static IMath createLambda(){
        return (x,y) -> x + y;
    }
}
