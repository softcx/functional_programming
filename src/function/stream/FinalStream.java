package function.stream;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 终止流操作
 *
 * @author 旭旭
 * @create 2018-08-17 23:32
 **/
public class FinalStream {
    public static void main(String[] args){
//        forEachOrdered();
//        collect();
//        reduce();
//        minMixCount();
        findFirst();
    }

    /**
     * forEachOrdered
     */
    public static void forEachOrdered(){
        IntStream.of(new int[]{1,2,3,4,5,6,7}).parallel().forEach(System.out::println);
//        IntStream.of(new int[]{1,2,3,4,5,6,7}).parallel().forEachOrdered(System.out::println);
    }

    /**
     * collect、toArray
     */
    public static void collect(){
        String s = "hello world!";
        List<String> collect = Stream.of(s.split(" ")).collect(Collectors.toList());
        System.out.println(collect);
    }

    /**
     * reduce
     */
    public static void reduce(){
        Integer[] intArr = new Integer[]{1,2,3,4,5,6,7,8,9,10};
        Optional<Integer> optional = Stream.of(intArr).reduce((x, y) -> x + y);
        System.out.println(optional.get());
    }

    /**
     * minMixCount
     */
    public static void minMixCount(){
        Integer[] intArr = new Integer[]{1,2,3,4,5,6,7,8,9,10};
        Optional<Integer> optional = Stream.of(intArr).max(Comparator.comparingInt(x -> x));
        System.out.println(optional.get());
    }

    //短路操作--------------------------------
    /**
     * findFirst
     */
    public static void findFirst(){
        Optional<Integer> first = Stream.generate(() -> new Random().nextInt()).findFirst();
        System.out.println(first.get());
    }
}
