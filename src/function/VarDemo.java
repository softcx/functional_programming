package function;

import java.util.function.Consumer;

/**
 * 变量引用
 *
 * @author 旭旭
 * @create 2018-08-12 1:01
 **/
public class VarDemo {

    public static void main(String[] args) {
        //JDK8前内部类使用外部变量，外部变量必须声明为final，是因为生命周期的原因。方法中的局部变量
        // ，方法结束后这个变量就要释放掉，而final会保证这个变量始终指向一个对象。如果外部类的方法中的变量不定义final
        // ，那么当外部类方法执行完毕的时候，这个局部变量肯定也就被GC了

        String str = "一个字符串";
//        str = "我变了！";
        Consumer<String> consumer = s -> System.out.println(s + str);
        consumer.accept("我是");
    }
}
