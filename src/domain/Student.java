package domain;

/**
 * ${DESCRIPTION}
 *
 * @author 旭旭
 * @create 2018-08-18 23:46
 **/
public class Student {
    private String name;
    private int age;
    private boolean gender;
    private String Grade;

    public Student() {
    }

    public Student(String name, int age, boolean gender, String grade) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        Grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", Grade='" + Grade + '\'' +
                '}';
    }
}
