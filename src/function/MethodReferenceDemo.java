package function;

import domain.Dog;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * ${DESCRIPTION}
 *
 * @author 旭旭
 * @create 2018-08-05 20:17
 **/
public class MethodReferenceDemo {
    public static void main(String[] args) {
        //消费者 方法引用模式
//        consumer();
        //静态方法引用
//        callStaticMethod();
        //非静态 实例方法引用
//        callMethod();
        //非静态 类方法引用
//        callMethodByClass();
        //构造函数方法引用
//        callConstructorMethod();
        //数据不变模式
        callMethod2();
    }

    public static void consumer(){
        Consumer<String> consumer = System.out::println;
        consumer.accept("我是一个消费者");
    }

    private static void callStaticMethod() {
        Consumer<Dog> consumer = Dog::bark;
        consumer.accept(new Dog());
    }

    private static void callMethod() {
        Dog dog = new Dog();
        Function<Integer,Integer> function = dog::eat;
        System.out.println("还剩[" + function.apply(3) + "]斤狗粮");
    }

    private static void callMethodByClass() {
        BiFunction<Dog,Integer,Integer> biFunction  = Dog::eat;
        System.out.println("还剩[" + biFunction.apply(new Dog(),4) + "]斤狗粮");
    }

    private static void callConstructorMethod() {
        Supplier<Dog> supplier = Dog::new;
        System.out.println("new 了一个对象" + supplier.get());
    }

    private static void callMethod2() {
        Dog dog = new Dog();
        Function<Integer,Integer> function = dog::eat; //函数声明
        dog = null;
        System.out.println("还剩[" + function.apply(3) + "]斤狗粮");
    }

}
