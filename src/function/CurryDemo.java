package function;

import java.util.function.Function;

/**
 * 级联表达式和柯里化
 *
 * @author 旭旭
 * @create 2018-08-12 1:09
 **/
public class CurryDemo {

    public static void main(String[] args) {
        //级联表达式
        Function<Integer,Function<Integer,Integer>> fun = x -> y -> x + y;
        //柯里化（Currying）是把接受多个参数的函数变换成接受一个单一参数(最初函数的第一个参数)的函数
        //柯里化的意义：函数标准化
        //高阶函数：返回函数的函数
        System.out.println(fun.apply(2).apply(3));

        Function<Integer,Function<Integer,Function<Integer,Integer>>> fun2 = x -> y -> z -> x + y + z;

    }
}
